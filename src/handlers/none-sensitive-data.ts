import { FastifyBaseLogger, FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { SensitiveDataInputType, SensitiveDataOutputType } from "../schemas";
import { IncomingMessage, Server, ServerResponse } from "http";
import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";

export const createNoneSensitiveDataHandler = (server: FastifyInstance<Server<typeof IncomingMessage, typeof ServerResponse>, IncomingMessage, ServerResponse<IncomingMessage>, FastifyBaseLogger, TypeBoxTypeProvider>) => {
    return async (request: FastifyRequest<{
        Body: SensitiveDataInputType;
        Reply: SensitiveDataOutputType;
    }>, reply: FastifyReply) => {
        var { supabase } = server
        let { data } = await supabase.from("non-sensitive-data").insert(request.body)

        reply.status(200).send({ data });
    }
}

export const getNoneSensitiveDataHandler = async (request: FastifyRequest<{
    Reply: SensitiveDataOutputType;
}>, reply: FastifyReply) => {
    var { supabase } = request.server
    let { data } = await supabase.from("non-sensitive-data").select()

    reply.status(200).send({ data });
}
