import { Type, TypeBoxTypeProvider } from "@fastify/type-provider-typebox"
import { FastifyBaseLogger, FastifyInstance } from "fastify"
import { IncomingMessage, Server, ServerResponse } from "http"
import { SensitiveDataInput, SensitiveDataInputType, SensitiveDataOutput, SensitiveDataOutputType } from "../schemas";
import { createSensitiveDataHandler, getSensitiveDataHandler } from "../handlers/sensitive-data";
import { verifySupabaseJWT } from "../hooks/verify-supabase-jwt";

export function sensitiveDataController(server:
  FastifyInstance<Server<typeof IncomingMessage, typeof ServerResponse>, IncomingMessage,
    ServerResponse<IncomingMessage>, FastifyBaseLogger, TypeBoxTypeProvider>, _: any, done: any) {

  server.addHook('preHandler', verifySupabaseJWT)

  // server.post<{ Body: SensitiveDataInputType, Reply: SensitiveDataOutputType }>(
  //   '/api/sensitive-data',
  //   {
  //     schema: {
  //       body: SensitiveDataInput,
  //       response: {
  //         200: SensitiveDataOutput
  //       },
  //       headers: {
  //         "Authorization": Type.String()
  //       }
  //     },
  //   },
  //   createSensitiveDataHandler(server)
  // )

  server.get<{ Reply: SensitiveDataOutputType }>(
    '/api/sensitive-data',
    {
      schema: {
        response: {
          200: SensitiveDataOutput
        },
        headers: {
          "Authorization": Type.String()
        }
      },
    },
    getSensitiveDataHandler
  )

  done()
}