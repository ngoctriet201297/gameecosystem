
import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox"
import { FastifyBaseLogger, FastifyInstance } from "fastify"
import { IncomingMessage, Server, ServerResponse } from "http"

export function authCallbackController(server: FastifyInstance<Server<typeof IncomingMessage, typeof ServerResponse>, IncomingMessage, ServerResponse<IncomingMessage>, FastifyBaseLogger, TypeBoxTypeProvider>, _: any , done: any){
    server.get('/callback', async (req, reply) => {
       reply.sendFile('callback.html')
      })
      done()
}
