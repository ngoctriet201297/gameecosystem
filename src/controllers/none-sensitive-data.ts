import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox"
import { FastifyInstance, FastifyBaseLogger } from "fastify"
import { Server, IncomingMessage, ServerResponse } from "http"
import { SensitiveDataInputType, SensitiveDataOutputType, SensitiveDataInput, SensitiveDataOutput } from "../schemas"
import { createNoneSensitiveDataHandler, getNoneSensitiveDataHandler } from "../handlers/none-sensitive-data"

export function noneSensitiveDataController(server:
  FastifyInstance<Server<typeof IncomingMessage, typeof ServerResponse>, IncomingMessage,
    ServerResponse<IncomingMessage>, FastifyBaseLogger, TypeBoxTypeProvider>, _: any, done: any) {

  // server.post<{ Body: SensitiveDataInputType, Reply: SensitiveDataOutputType }>(
  //   '/api/none-sensitive-data',
  //   {
  //     schema: {
  //       body: SensitiveDataInput,
  //       response: {
  //         200: SensitiveDataOutput
  //       }
  //     },
  //   },
  //   createNoneSensitiveDataHandler(server)
  // )

  server.get<{ Reply: SensitiveDataOutputType }>(
    '/api/none-sensitive-data',
    {
      schema: {
        response: {
          200: SensitiveDataOutput
        }
      },
    },
    getNoneSensitiveDataHandler
  )

  done()
}