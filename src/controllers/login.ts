import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox"
import { FastifyBaseLogger, FastifyInstance } from "fastify"
import { IncomingMessage, Server, ServerResponse } from "http"

export function loginController(server: FastifyInstance<Server<typeof IncomingMessage, typeof ServerResponse>, IncomingMessage, ServerResponse<IncomingMessage>, FastifyBaseLogger, TypeBoxTypeProvider>, _: any, done: any) {
    server.get('/login', async (request, reply) => {
        const { supabase } = server
        let obj = await supabase.auth.signIn({
            provider: 'discord',
        }, {
            redirectTo: 'http://localhost:8080/public/callback.html'
        })

        reply.redirect(302, obj.url!)

        supabase.auth.getSessionFromUrl()
    })
    done();
}