import { Static, Type } from '@sinclair/typebox'

export const SensitiveDataInput = Type.Object({
  name: Type.Optional(Type.String()),
})

export type SensitiveDataInputType = Static<typeof SensitiveDataInput>


export const SensitiveDataOutput = Type.Object({
  id:  Type.Optional(Type.Integer()),
  name: Type.Optional(Type.String()),
  created_at:  Type.Optional(Type.Date()),
})

export type SensitiveDataOutputType = Static<typeof SensitiveDataOutput>
