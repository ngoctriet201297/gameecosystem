export const configurationSchema = {
    type: 'object',
    required: ['SUPABASE_KEY', 'SUPABASE_URL'],
    properties: {
        SUPABASE_KEY: {
            type: 'string'
        },
        SUPABASE_URL: {
            type: 'string'
        }
    }
}