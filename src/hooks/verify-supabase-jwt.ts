import { FastifyReply, FastifyRequest } from "fastify";

export const verifySupabaseJWT = async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      if (!request.headers.authorization) {
        throw new Error('No token was sent');
      }
      const { supabase } = request.server

      const jwt = request.headers!.authorization!.replace('Bearer ', '');
      let user = await supabase.auth.api.getUser(jwt)
      if (user.error) {
        throw new Error(user.error.message);
      }
      (request as any).user = user;
      (request as any).token = jwt;
      supabase.auth.setAuth(jwt)
    } catch (error) {
      reply.code(401).send(error);
    }
  }