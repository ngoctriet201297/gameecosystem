import fastify from 'fastify'
import helmet from '@fastify/helmet'
import { TypeBoxTypeProvider } from '@fastify/type-provider-typebox'
import fastifySupabase from 'fastify-supabase'
import { loginController } from './controllers/login'
import { authCallbackController } from './controllers/auth-callback'
import { sensitiveDataController } from './controllers/sensitive-data'
import { noneSensitiveDataController } from './controllers/none-sensitive-data'
import fastifyStatic from '@fastify/static'
import path from 'path'
import fastifySwagger from '@fastify/swagger'
import fastifySwaggerUi from '@fastify/swagger-ui'
import fastifyEnv from '@fastify/env'
import { configurationSchema } from './schemas/configuration'

async function StartUp() {

  const server = fastify({
    logger: true
  }).withTypeProvider<TypeBoxTypeProvider>()

  await server.register(fastifyEnv, {
    confKey: 'config',
    dotenv: true,
    data: process.env,
    schema: configurationSchema
  });

  const config = (server as any).config
  server.register(helmet)
  server.register(fastifyStatic, {
    root: path.join(__dirname, 'public'),
    prefix: '/public/', // optional: default '/'
  })

  server.register(fastifySupabase, {
    supabaseKey: config.SUPABASE_KEY,
    supabaseUrl: config.SUPABASE_URL
  })

  server.register(fastifySwagger)
  server.register(fastifySwaggerUi)
  server.register(sensitiveDataController)
  server.register(noneSensitiveDataController)
  server.register(loginController)
  server.register(authCallbackController)


  await server.listen({ port: 8080 }, (err, address) => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
    console.log(`Server listening at ${address}`)
  })
}

(async () => {
  await StartUp()
})()